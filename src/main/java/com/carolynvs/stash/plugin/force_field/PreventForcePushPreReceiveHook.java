package com.carolynvs.stash.plugin.force_field;

import com.atlassian.bitbucket.commit.CommitService;
import com.atlassian.bitbucket.i18n.I18nService;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.bitbucket.hook.HookResponse;
import com.atlassian.bitbucket.hook.PreReceiveHook;
import com.atlassian.bitbucket.hook.repository.RepositoryHook;
import com.atlassian.bitbucket.hook.repository.RepositoryHookContext;
import com.atlassian.bitbucket.hook.repository.RepositoryHookService;
import com.atlassian.bitbucket.repository.RefChange;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.setting.Settings;
import com.atlassian.bitbucket.permission.Permission;
import com.atlassian.bitbucket.user.SecurityService;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Nonnull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Jim Bethancourt
 */
public class PreventForcePushPreReceiveHook implements PreReceiveHook {

    private static final Logger log = LoggerFactory.getLogger(PreventForcePushPreReceiveHook.class);

    private final PreventForcePushHook preventForcePushHook;
    private final PluginSettingsFactory pluginSettingsFactory;
    private final SecurityService securityService;
    private final RepositoryHookService repositoryHookService;

    public PreventForcePushPreReceiveHook(CommitService commitService, I18nService i18nService,
                                          PluginSettingsFactory pluginSettingsFactory,
                                          SecurityService securityService,
                                          RepositoryHookService repositoryHookService) {
        this.pluginSettingsFactory = pluginSettingsFactory;
        this.preventForcePushHook = new PreventForcePushHook(commitService, i18nService);
        this.securityService = securityService;
        this.repositoryHookService = repositoryHookService;
    }

    @Override
    public boolean onReceive(@Nonnull final Repository repository, @Nonnull Collection<RefChange> refChanges, @Nonnull HookResponse hookResponse) {

        RepositoryHook hook = securityService.withPermission(Permission.REPO_ADMIN, "Get plugin configuration")
                .call(() -> repositoryHookService.getByKey(repository, "com.carolynvs.force-field:force-field.hook"));

        Settings settings = repositoryHookService.createSettingsBuilder().build(); // generate a default settings object
        if (hook.isEnabled() && hook.isConfigured()) {
            // Repository hook is configured and enabled.
            // Repository hook overrides default pre-receive hook configuration
            log.debug("PreReceiveRepositoryHook configured. Skip PreReceiveHook");
            return true;
        } else {
            // Repository hook not configured
            log.debug("PreReceiveRepositoryHook not configured. Run PreReceiveHook");
            PluginSettings pluginSettings = pluginSettingsFactory.createGlobalSettings();

            Map<String, String> settingsMap =
                    (HashMap<String, String>) pluginSettings.get(ForceFieldConfigServlet.SETTINGS_MAP);

            Settings storedConfig = buildForceFieldConfig(settingsMap);

            if (storedConfig != null) {
                settings = storedConfig;
            }
        }

        return preventForcePushHook.onReceive(new RepositoryHookContext(repository, settings), refChanges, hookResponse);
    }

    Settings buildForceFieldConfig(Map<String, String> settingsMap) {
        HashMap<String, Object> config = new HashMap<>();

        if(settingsMap != null) {
            for (String fieldName : settingsMap.keySet()) {
                addFieldValueToPluginConfigMap(settingsMap, config, fieldName);
            }
        }

        return repositoryHookService.createSettingsBuilder().addAll(config).build();
    }

    void addFieldValueToPluginConfigMap(Map<String, String> settingsMap, HashMap<String, Object> config, String fieldName) {
        String value = settingsMap.get(fieldName);
        if (value != null && (value.equals("on") || value.equals("true"))) { // handle "on" value
            config.put(fieldName, true);
        } else if (value != null && !value.isEmpty()) {
            config.put(fieldName, value);
        }
    }

}
